species = ['hum', 'mou', 'opo']

rule parse_intronic_sce:
    input:
        rds = "/home/kleiss/ws/cerebellum_analysis/data/sce/updated/{species}_sce.rds",
        script = "scripts/prepare_single_nuc_data.R"
    output: "www/sce_intronic/{species}_sce.rds"
    threads: 1
    shell:
        '''
        Rscript --vanilla \
            scripts/prepare_single_nuc_data.R \
            {input.rds} {output}
        '''

rule parse_exonic_sce:
    input:
        rds = "/home/kleiss/ws/cerebellum_analysis/data/sce/updated_exonic/{species}_exonic_sce.rds",
        script = "scripts/prepare_single_nuc_data.R"
    output: "www/sce_exonic/{species}_sce.rds"
    threads: 1
    shell:
        '''
        Rscript --vanilla \
            scripts/prepare_single_nuc_data.R \
            {input.rds} {output}
        '''

rule save_all_data:
    input:
        expand("www/sce_{counting}/{species}_sce.rds", counting = ['exonic', 'intronic'], species = species),
        script = 'scripts/save_all_sc_data.R'
    output:
        'data/all_sc_data.rds'
    shell:
        '''
        Rscript --vanilla \
           {input.script}
        '''

rule all_sce:
    input:
        expand("www/sce_exonic/{species}_sce.rds", species = species),
        expand("www/sce_intronic/{species}_sce.rds", species = species)
