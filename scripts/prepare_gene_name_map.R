library(tidyverse)
library(SingleCellExperiment)

scl <- readRDS("data/all_sc_data.rds")

gene_map <- list(
    human = list(
        premrna = list(
            genes = rownames(scl$HUM$`pre-mRNA`),
            names = rowData(scl$HUM$`pre-mRNA`)$gene_name
        ),
        exonic = list(
            genes = rownames(scl$HUM$exonic),
            names = rowData(scl$HUM$exonic)$gene_name
        )
    ),
    mouse = list(
        premrna = list(
            genes = rownames(scl$MOU$`pre-mRNA`),
            names = rowData(scl$MOU$`pre-mRNA`)$gene_name
        ),
        exonic = list(
            genes = rownames(scl$MOU$exonic),
            names = rowData(scl$MOU$exonic)$gene_name
        )
    ),
    opossum = list(
        premrna = list(
            genes = rownames(scl$OPO$`pre-mRNA`),
            names = rowData(scl$OPO$`pre-mRNA`)$gene_name
        ),
        exonic = list(
            genes = rownames(scl$OPO$exonic),
            names = rowData(scl$OPO$exonic)$gene_name
        )
    )
)

saveRDS(gene_map, "data/gene_map.rds")
